</DOCTYPE html>
<html>
<head>
     <title> File Sharing</title>
     <style>
         body{
             background-image: url("51953.jpg");
             background-repeat: x-repeat;
         }
         div{
             text-align: center;
             position: absolute;
             height: 200px;
             width: 400px;
             margin: -100px 0 0 -200px;
             top: 50%;
             left: 50%;
             
         }
     </style>
</head>
<body>
       <div>
       <h2>Login success!</h2>
       <h5>Please upload your file</h5>       
       <p>
       <form method = "GET">
       <input type = "submit" name = "upload" value = "upload">
       <input type = "submit" name = "logout" value = "logout">
       <input type = "submit" name = "resetpassword" value="reset">
       </div>   
       </form>
       </p>
<?php

    session_start();

    $username = $_SESSION['username'];

     if($username != null){

       if( !preg_match('/^[\w_\.\-]+$/',$username)){
             echo "Invalid username";
             exit;
        }

        //$full_path = sprintf("/srv/uploads");
        $full_path = sprintf("/srv/uploads/%s/",  $username);
        $share_path = sprintf("/srv/uploads/publicfolder/");
        
        $files = scandir($full_path);
        $docs = scandir($share_path);
        
        echo "File name: ";
        echo "<br>";
        echo "<br>";
        foreach($files as $file){
           if($file != "." && $file != ".."){
             echo  basename($file)."  ";
             echo "<a href =\"./download.php?id=$file \">open\n\n </a>";
             echo "  ";
             echo "<a href =\"./delete.php?id=$file \">delete\n\n </a>";
             echo "<a href =\"./share.php?id=$file \">share all\n\n </a>";
             echo  "<br>";
           }
        }
        echo "Share Files: ";
        echo "<br>";
        echo "<br>";
        foreach($docs as $doc){
           if($doc != "." && $doc != ".."){
             echo  basename($doc)."  ";
             echo "<a href =\"./download_share.php?id=$doc \">open\n\n </a>";
             echo "  ";
             echo "<a href =\"./delete_share.php?id=$doc \">delete\n\n </a>";
             echo  "<br>";
           }
        }
     }


       if(isset($_GET['upload'])){
                if($_GET['upload'] == "upload"){
              header("Location: upload.php");
              exit;
         }
       }else{

          $_GET['upload'] = null;
       }

       if(isset($_GET['logout'])){
           if($_GET['logout'] == "logout"){
                header("Location: logout.php");
                exit;
           }
       }else{
           $_GET['logout'] =  null;
       }

       if(isset($_GET['resetpassword'])) {
              if($_GET['resetpassword'] == "reset"){
                  header("Location: resetpw.php");
                  exit;
              }
       }else{
           $_GET['resetpassword'] = null;
       }
       
       echo "Last modified: " . date ("F d Y H:i:s.", getlastmod());

?>
</body>
</html>
