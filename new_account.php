<!DOCTYPE html>
<html>
<head>
	<title>New account!</title>
</head>

<body>

<?php
        $userdata = fopen("userdata.txt", "a");
	$new_username = $_POST["new_username"];
	$new_password = $_POST["new_password"];
	$txt = $new_username." ".$new_password."\n";
	fwrite($userdata, $txt);
	fclose($userdata);

	$path = sprintf("/srv/uploads/%s",$new_username);
	if(mkdir($path, 0777)){
		header("Location: login.php");
		exit;
	}
?>

</body>
</html>
