<?php
       session_start();

       $_GET['input'] = isset($_GET['input'])? $_GET['input'] : null;
       $filename =  isset($_GET['id'])? $_GET['id'] : null;

       if( !preg_match('/^[\w_\.\-]+$/', $filename)){
                    echo "Invalid filename";
                    exit;
       }

       $username = $_SESSION['username'];

       if( !preg_match('/^[\w_\-]+$/',$username)){
                    echo "Invalid username";
                    exit;
       }

       $full_path = sprintf("/srv/uploads/publicfolder/%s", $filename);

       $finfo = new finfo(FILEINFO_MIME_TYPE);
       $mime = $finfo ->file($full_path);
       
       header("Content-Type: ".$mime);
       readfile($full_path);
       

?>