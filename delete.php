<?php
   session_start();

   $filename = isset($_GET['id'])? $_GET['id']:null;

   $username = $_SESSION['username'];

   $full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

   if(unlink($full_path)){
     header("Location: fileshare.php");
     exit;
   }

?>
