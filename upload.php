</DOCTYPE html>
<html>
<head>
      <title>Upload Page</title>
</head>
<body>
     <form enctype = "multipart/form-data" action = "upload.php" method = "POST">
        <p>
           <input type = "hidden" name = "MAX_FILE_SIZE" value = "20000000" />
           <label for = "uploadfile_input">Choose a file to upload: </label> <input name = "uploadedfile" type = "file" id = "uploadfile_input" />
        </p>
        <p>
           <input type = "submit"  name = "input" value = "Upload File" />
        </p>
     </form>

<?php
      
      session_start();

      $ans = isset($_POST['input'])? $_POST['input'] : null;

      if($ans =="Upload File"){
        
        $filename = isset($_FILES['uploadedfile']['name']) ? basename($_FILES['uploadedfile']['name']):null;
        
        $filename = basename($_FILES['uploadedfile']['name']);

        if( !preg_match('/^[\w_\.\-]+$/', $filename)){
              echo "Invalid filename";
              exit;
        }

        $username = $_SESSION['username'];

        if( !preg_match('/^[\w_\.\-]+$/', $username)){
             echo "Invalid username";
             exit;
        }
        
        $full_path = sprintf("/srv/uploads/%s/%s",$username,$filename);
        //$full_path = sprintf("/srv/uploads/%s",$filename);
        if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path)){
        
               echo "upload success";
               header("Location: fileshare.php");
               exit;
         }else{
               echo "upload failure,please try again";
             //  exit;
         }


        
      }
?>
</body>
</html>

